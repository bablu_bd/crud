<?php
include_once"../../vendor/autoload.php";
use App\Names\Names;
session_start();
$obj= new Names();


if(!empty($_POST['title'])) {
    if(preg_match("/([a-z*0-9_A-Z])/",$_POST['title'])){
        $_POST['title']=filter_var($_POST['title'],FILTER_SANITIZE_STRING);
        $obj->setData($_POST)->update();
    }else{
        $_SESSION['message']= "input invalid";
        header('location:create.php');
    }

}else{
    $_SESSION['message']= "input can't be empty";
    header('location:create.php');
}
?>