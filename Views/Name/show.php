<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
include_once"../../vendor/autoload.php";
use App\Names\Names;
session_start();
$obj= new Names();
$value = $obj->setData($_GET)->show();

?>
<html>
<head>
    <title>list of student</title>
</head>
<body>
<a href="index.php">Back To List</a>
<table border="1">
    <tr>
        <td>Serial</td>
        <td>Title</td>
        <td>Photo</td>
        <td>Action</td>
    </tr>


        <tr>
            <td><?php echo $value['id'] ?></td>
            <td><?php echo $value['title'] ?></td>
            <td><img src="<?php echo"images/".$value['image']?>"alt="no image" height="100px" width="100px"> </td>
            <td><a href="edit.php?id=<?php echo $value['id']?>">Edit</a></td>
        </tr>

</table>
</body>
</html>
