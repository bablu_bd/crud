<?php

include_once"../../vendor/autoload.php";
use App\Names\Names;
session_start();
$obj= new Names();
$error = array();
$image_name = $_FILES['image']['name'];
$image_type = $_FILES['image']['type'];
$image_tmp_location = $_FILES['image']['tmp_name'];
$image_size = $_FILES['image']['size'];
$my_image_extension = strtolower(end(explode('.',$image_name)));
$required_format = array('jpg','jpeg','png','gif');

if(in_array($my_image_extension,$required_format)){
    echo "image found";
}else{
    $error[] = "Invalid file format";
}
if($image_size > 2000000){
    $error[]="image size too large";

}
if(empty($error)){
    move_uploaded_file($image_tmp_location,"images/".$image_name);
    $_POST['image']= $image_name;
    $obj->setData($_POST);
    $obj->store();
}

if(!empty($_POST['title'])) {
    if(preg_match("/([a-z*0-9_A-Z])/",$_POST['title'])){
        $_POST['title']=filter_var($_POST['title'],FILTER_SANITIZE_STRING);
        $obj->setData($_POST)->store();
    }else{
        $_SESSION['message']= "input invalid";
        header('location:create.php');
    }

}else{
    $_SESSION['message']= "input can't be empty";
    header('location:create.php');
}
?>


