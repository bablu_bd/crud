<?php
include_once"../../vendor/autoload.php";
use App\Names\Names;
$obj= new Names();
$allData = $obj->index();
session_start();
if(isset($_SESSION['message'])){
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
<html>
<head>
    <title>list of student</title>
</head>
<body>
<a href="create.php">Add New</a>
    <table border="1">
        <tr>
            <td>Serial</td>
            <td>Title</td>
            <td>Photo</td>
            <td>Action</td>
        </tr>
        <?php
        $serial = 1;
        foreach($allData as $key => $value){
        ?>
        <tr>
            <td><?php echo $serial++ ?></td>
            <td><?php echo $value['title'] ?></td>
            <td><img src="<?php echo"images/".$value['image']?>"alt="no image" height="100px" width="100px"> </td>
            <td><a href="show.php?id=<?php echo $value['id']?>">View Details</a></td>
            <td><a href="delete.php?id=<?php echo $value['id']?>">Delete</a></td>
        </tr>
        <?php } ?>
    </table>
</body>
</html>
<a href="images"